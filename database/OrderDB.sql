/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50139
Source Host           : localhost:3306
Source Database       : order_db

Target Server Type    : MYSQL
Target Server Version : 50139
File Encoding         : 65001

Date: 2010-09-28 14:57:35
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `leave_words`
-- ----------------------------
DROP TABLE IF EXISTS `leave_words`;
CREATE TABLE `leave_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(11) NOT NULL,
  `taste` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `is_use` varchar(1) DEFAULT '0',
  `reversion` varchar(1000) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leave_words
-- ----------------------------

-- ----------------------------
-- Table structure for `order_log`
-- ----------------------------
DROP TABLE IF EXISTS `order_log`;
CREATE TABLE `order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(11) NOT NULL,
  `desk_id` varchar(11) NOT NULL,
  `login_id` varchar(11) NOT NULL,
  `op_type` varchar(10) DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_log
-- ----------------------------
INSERT INTO `order_log` VALUES ('1', '2', '009', 'abc', '2', '2010-09-23 13:32:40');
INSERT INTO `order_log` VALUES ('2', '1', '013', 'abc', '2', '2010-09-23 13:34:48');
INSERT INTO `order_log` VALUES ('3', '9', '008', 'abc', '2', '2010-09-23 13:35:20');
INSERT INTO `order_log` VALUES ('4', '2', '045', 'abc', '2', '2010-09-23 13:37:31');
INSERT INTO `order_log` VALUES ('5', '1', '098', 'abc', '2', '2010-09-23 14:48:04');
INSERT INTO `order_log` VALUES ('6', '1', '002', 'abc', '2', '2010-09-23 16:06:47');
INSERT INTO `order_log` VALUES ('7', '1', '002', 'abc', '2', '2010-09-23 16:07:17');
INSERT INTO `order_log` VALUES ('8', '2', '011', 'abc', '2', '2010-09-23 16:31:18');
INSERT INTO `order_log` VALUES ('9', '3', '001', 'abc', '2', '2010-09-23 18:15:05');
INSERT INTO `order_log` VALUES ('10', '8', '012', 'abc', '2', '2010-09-23 19:36:19');
INSERT INTO `order_log` VALUES ('11', '2', '099', 'abc', '2', '2010-09-27 14:10:47');
INSERT INTO `order_log` VALUES ('12', '1', '052', '2-5', '2', '2010-09-28 12:59:19');
INSERT INTO `order_log` VALUES ('13', '1', '093', '2-5', '2', '2010-09-28 13:00:10');
INSERT INTO `order_log` VALUES ('14', '1', '093', '2-5', '2', '2010-09-28 13:01:31');
INSERT INTO `order_log` VALUES ('15', '1', '093', 'abc', '1', '2010-09-28 13:02:46');
INSERT INTO `order_log` VALUES ('16', '4', '099', 'abc', '0', '2010-09-28 13:03:07');
INSERT INTO `order_log` VALUES ('17', '3', '099', 'abc', '0', '2010-09-28 13:03:18');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `order_version` varchar(10) DEFAULT '0',
  `image_path` varchar(255) DEFAULT NULL,
  `price` varchar(20) NOT NULL,
  `is_delete` varchar(1) DEFAULT '0',
  `update_at` varchar(20) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '宫保鸡丁', '宫保鸡丁，黔菜传统名菜，由鸡丁、干辣椒、花生米等炒制而成。由于其入口鲜辣，鸡肉的鲜嫩配合花生的香脆，广受大众欢迎。', 'recai', '5', 'file/images/1285640632140.png', '15', '0', '2010-09-28 10:23:52', '2010-09-21 13:08:19');
INSERT INTO `orders` VALUES ('2', '酱爆鸡丁', '京酱是北京的一个特色产品，由此衍生出很多以酱爆命名的菜，酱爆鸡丁就是最典型的一个。', 'recai', '5', 'file/images/1285567789075.png', '15', '0', '2010-09-27 14:09:49', '2010-09-21 13:12:32');
INSERT INTO `orders` VALUES ('3', '鱼香肉丝', '鱼香肉丝是一道常见川菜。鱼香，是四川菜肴主要传统味型之一。成菜具有鱼香味，其味是调味品调制而成。', 'recai', '5', 'file/images/1285567700059.png', '15', '0', '2010-09-27 14:08:20', '2010-09-21 13:13:22');
INSERT INTO `orders` VALUES ('4', '拍黄瓜', '做法：将黄瓜洗净，放在案板上用刀拍开，顺长切成两半，然后采用抹刀法切成小抹刀块，放入盆内，拌入精盐、蒜泥、香油即成。食时盛盘内淋入少许醋。 ', 'liangcai', '7', 'file/images/1285640626484.png', '10', '0', '2010-09-28 10:23:46', '2010-09-21 13:14:03');
INSERT INTO `orders` VALUES ('5', '凉拌菠菜', '做法: 菠菜2斤，洗净后，切段。用炉子上沸水焯一下，待用。作料: 食盐,味精少许，蒜粉，香油一起搅拌均匀。最后可以放点琥珀花生仁。', 'liangcai', '5', 'file/images/1285640620578.png', '10', '0', '2010-09-28 10:23:40', '2010-09-21 13:15:06');
INSERT INTO `orders` VALUES ('6', '酱牛肉', '酱牛肉一种菜名，有补中益气、滋养脾胃、强健筋骨、化痰息风、止渴止涎的功效，适用于中气下陷、气短体虚，筋骨酸软、贫血久病及面黄目眩之人食用。', 'liangcai', '6', 'file/images/1285640615734.png', '20', '0', '2010-09-28 12:56:51', '2010-09-21 13:15:49');
INSERT INTO `orders` VALUES ('7', '拉面', '拉面，是西北城乡独具地方风味的面食名吃。拉面可以蒸、煮、烙、炸、炒，各有一番风味。拉面是甘肃省兰州的名产。', 'mianshi', '6', 'file/images/1285640608046.png', '10', '0', '2010-09-28 12:56:37', '2010-09-21 13:16:18');
INSERT INTO `orders` VALUES ('8', '炒面片', '在面片已下锅的同时 用炒锅炒好肉丁或肉片及辣子、笋片、菜瓜等蔬菜或粉条、木耳，待面片开锅，即用漏勺捞出面片，放到炒好的菜中翻炒，略炒片刻，即可出锅食用。', 'mianshi', '8', 'file/images/1285640603265.png', '12', '0', '2010-09-28 11:03:02', '2010-09-21 13:17:16');
INSERT INTO `orders` VALUES ('9', '意大利面', '意大利面，又称之为意粉，是西餐品种中最接近中国人饮食习惯，最容易被接受的。', 'mianshi', '8', 'file/images/1285640598093.png', '10', '0', '2010-09-28 12:56:09', '2010-09-21 13:17:40');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nikename` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `gender` varchar(2) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'abc', '123', 'joselee', 'lixa8@126.com', '15010283469', 'M', '2010-09-28 12:46:59');

-- ----------------------------
-- Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `overdue` varchar(1) DEFAULT '0',
  `update_at` varchar(20) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES ('1', '啊啊啊啊', '3333333333333', 'file/videoes/1285568177372.3gp', '0', null, '2010-09-27 14:16:17');
